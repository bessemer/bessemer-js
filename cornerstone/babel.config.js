module.exports = {
    presets: [
        ["@babel/env", {
            targets: {
                browsers: ["> 1%", "last 2 versions"]
            }
        }]
    ],
    plugins: [
        '@babel/plugin-syntax-import-meta',
        '@babel/plugin-proposal-class-properties',
        ['@babel/plugin-proposal-decorators', {
            decoratorsBeforeExport: true
        }],
        '@babel/plugin-proposal-export-namespace-from',
        '@babel/plugin-proposal-numeric-separator',
        ['babel-plugin-root-import', {
            paths: [{
                rootPathSuffix: './src/main/js',
                rootPathPrefix: '~/'
            }, {
                rootPathSuffix: './src',
                rootPathPrefix: '!/'
            }],
        }]
    ]
};