import { normalizePhoneNumber } from '~/phone';

test('test normalizePhoneNumber simple case', () => {
    expect(normalizePhoneNumber('+852 6569-8900')).toBe('+85265698900');
    expect(normalizePhoneNumber('+1(817) 569-8900')).toBe('+18175698900');
    expect(normalizePhoneNumber('(817) 569-8900')).toBe('8175698900');
    expect(normalizePhoneNumber('+44 (0)20 1245 7213')).toBe('+4402012457213');
    expect(normalizePhoneNumber('+1(817) 569-8900')).toBe('+18175698900');
    expect(normalizePhoneNumber('6123-6123')).toBe('61236123');
});

test('test normalizePhoneNumber extensions', () => {
    expect(normalizePhoneNumber('+852 6569-8900x1234')).toBe('+85265698900;ext=1234');
    expect(normalizePhoneNumber('+1(817) 569-8900;ext=1234')).toBe('+18175698900;ext=1234');
    expect(normalizePhoneNumber('(817) 569-8900;ext=1234;blah=1234')).toBe('8175698900;ext=1234;blah=1234');
    expect(normalizePhoneNumber('+44 (0)20 1245 7213;')).toBe('+4402012457213;');
    expect(normalizePhoneNumber('+1(817) 569-8900x1234;ext=3456')).toBe('+18175698900;ext=3456');
    expect(normalizePhoneNumber('6123-6123x12')).toBe('61236123;ext=12');
});

test('test multiple plus', () => {
    expect(normalizePhoneNumber('+44 (0)20 +1245 72+13')).toBe('+4402012457213');
});