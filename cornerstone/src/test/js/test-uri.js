import { parseUri, parseUrl, parseHref, resolveHref, empty } from '~/url';

test('test basic path parsing', () => {
    let urlString = '/first';

    let url = parseUrl(urlString);

    let expected = empty();
    expected.rawUrl = urlString;
    expected.location.rawPath = '/first';
    expected.location.path.push('first');

    expect(url).toStrictEqual(expected);
});

test('test basic path parsing href', () => {
    let urlString = 'first';

    let url = parseHref(urlString);

    let expected = empty();
    expected.rawUrl = urlString;
    expected.location.relative = true;
    expected.location.rawPath = 'first';
    expected.location.path.push('first');

    expect(url).toStrictEqual(expected);
});

test('test parse url isolated fragment', () => {
    let urlString = '#fragment';

    let expected = empty();
    expected.rawUrl = urlString;
    expected.location.relative = true;
    expected.location.fragment = 'fragment';

    expect(parseUrl(urlString)).toStrictEqual(expected);
    expect(parseHref(urlString)).toStrictEqual(expected);
});

test('test parse url isolated query', () => {
    let urlString = '?one=1&two=2';

    let expected = empty();
    expected.rawUrl = urlString;
    expected.location.relative = true;
    expected.location.query = 'one=1&two=2';
    expected.location.parameters = {one: '1', two: '2'};

    expect(parseUrl(urlString)).toStrictEqual(expected);
    expect(parseHref(urlString)).toStrictEqual(expected);
});

test('test parse url ambiguous host - amazon.com', () => {
    let urlString = 'amazon.com';

    let url = parseUrl(urlString);

    let expected = empty();
    expected.rawUrl = urlString;
    expected.authority = {};
    expected.authority.authentication = null;
    expected.authority.host = {};
    expected.authority.host.name = 'amazon.com';
    expected.authority.host.port = null;
    expected.authority.host.domain = ['com', 'amazon'];

    expect(url).toStrictEqual(expected);
});

test('test parse href ambiguous host - amazon.com', () => {
    let urlString = 'amazon.com';

    let url = parseHref(urlString);

    let expected = empty();
    expected.rawUrl = urlString;
    expected.location.relative = true;
    expected.location.rawPath = 'amazon.com';
    expected.location.path.push('amazon.com');

    expect(url).toStrictEqual(expected);
});

test('test parse url ambiguous host - localhost:3000/foo', () => {
    let urlString = 'localhost:3000/foo';

    let url = parseUrl(urlString);

    let expected = empty();
    expected.rawUrl = urlString;
    expected.authority = {};
    expected.authority.authentication = null;
    expected.authority.host = {};
    expected.authority.host.name = 'localhost';
    expected.authority.host.port = '3000';
    expected.authority.host.domain = ['localhost'];

    expected.location.rawPath = '/foo';
    expected.location.path.push('foo');

    expect(url).toStrictEqual(expected);
});

test('test parse href ambiguous host - localhost:3000/foo', () => {
    let urlString = 'localhost:3000/foo';

    let url = parseHref(urlString);

    let expected = empty();
    expected.rawUrl = urlString;
    expected.protocol = 'localhost';
    expected.location.rawPath = '3000/foo';
    expected.location.path.push('3000', 'foo');


    expect(url).toStrictEqual(expected);
});

test('test parse uri ambiguous host - localhost:3000/foo', () => {
    let urlString = 'localhost:3000/foo';

    let url = parseUri(urlString);

    let expected = empty();
    expected.rawUrl = urlString;
    expected.protocol = 'localhost';
    expected.location.rawPath = '3000/foo';
    expected.location.path.push('3000', 'foo');


    expect(url).toStrictEqual(expected);
});

test('test resolveHref', () => {
    expect(resolveHref('/one/two/three', 'four')).toStrictEqual(parseHref('/one/two/four'));
    expect(resolveHref('http://example.com', '/one')).toStrictEqual(parseHref('http://example.com/one'));
    expect(resolveHref('http://example.com/one/foo', '/two')).toStrictEqual(parseHref('http://example.com/two'));
    expect(resolveHref('http://example.com', 'one')).toStrictEqual(parseHref('http://example.com/one'));
    expect(resolveHref('http://example.com/one/foo', 'two')).toStrictEqual(parseHref('http://example.com/one/two'));
    expect(resolveHref('http://example.com/one/foo', 'http://example.com/one')).toStrictEqual(parseHref('http://example.com/one'));
    expect(resolveHref('http://example.com/one/foo', 'http://example2.com/one')).toStrictEqual(parseHref('http://example2.com/one'));
});