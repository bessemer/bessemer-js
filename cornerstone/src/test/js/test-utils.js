import { replaceAll, randomUuid } from '~/util';

test('test replaceAll', () => {
    expect(replaceAll('1234+5678+9', '+', '-')).toBe('1234-5678-9');
});

test('test randomUuid', () => {
    randomUuid();
});