import { isEmpty, join, map, split, isPresent, splitFirst, splitLast, filter, find, contains, startsWith, removeAll, DIGITS } from '~/util';

const charactersToKeep = [...DIGITS, '+'];

export const normalizePhoneNumber = (phone) => {
    const [phonePart, parameterPart] = splitFirst(phone, ';');
    const [numberPart, extensionPart] = splitLast(phonePart, 'x');

    let normalizedNumber = join(filter(numberPart, character => contains(charactersToKeep, character)), '');
    const hasPlus = startsWith(normalizedNumber, '+');

    normalizedNumber = removeAll(normalizedNumber, ['+']);

    let parameters = map(split(parameterPart, ';'), parameter => splitFirst(parameter, '='));

    // If we couldn't find an extension in the parameters section, lets try to find one in the AAA AAA AAAAxZZZZ format
    const extensionParameter = find(parameters, ([key, _]) => key === 'ext' );
    if(!isPresent(extensionParameter) && isPresent(extensionPart)) {
        parameters.push(['ext', extensionPart]);
    }

    let normalized = normalizedNumber;
    if(hasPlus) {
        normalized = '+' + normalized;
    }
    if(!isEmpty(parameters)) {
        normalized = normalized + ';';
    }

    normalized = normalized + join(map(parameters, parameter => join(parameter, '=')), ';');

    return normalized;
};