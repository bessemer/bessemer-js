import produce from 'immer';
import { isEmpty, join, map, isArray, startsWith, first, split, isNil, each, isString, reverse, emptyToNull, isPresent, isBlank, defaultString, splitFirst, splitLast, contains, defaults, removeStart, isObject, take } from '~/util';

export function encode(urlString) {
    return encodeURIComponent(urlString);
}

export function decode(urlString) {
    return decodeURIComponent(urlString);
}

export function decodeRequestParameter(urlString) {
    return decode(urlString.replace(/\+/g, '%20'));
}

export const uriComponents = {
    PROTOCOL: 'protocol',
    AUTHORITY: 'authority',
    LOCATION: 'location',
    PATH: 'path',
    PARAMETERS: 'parameters',
    FRAGMENT: 'fragment',
};

export function constructUrl(rawUrl = null, protocol = null, authority = null, location = {rawPath: '', relative: false, path: [], query: null, parameters: {}, fragment: null}) {
    return {rawUrl, protocol, authority, location};
}

function parseProtocol(urlString, parseOptions) {
    let [potentialProtocol, rest] = splitFirst(urlString, ':');

    // No : means no protocol is possible
    if(!isPresent(rest)) {
        return [null, urlString];
    }

    // This means the string started with :, so no protocol. We'll go ahead and remove the : from consideration
    if(isEmpty(potentialProtocol)) {
        return [null, rest];
    }

    // This means we have a host and therefore we have correctly captured the protocol
    if(startsWith(rest, '//')) {
        return [potentialProtocol, rest];
    }

    // Things get challenging here. If we're supporting "opaque" urls - that is urls which are not technically syntactically valid
    // then we assume that we are looking at a host/port pair
    if(parseOptions.opaque) {
        return [null, urlString];
    }

    // Otherwise, we need to support valid urns and can't tell the difference between an incomplete url and a urn, so since opaque is false
    // we assume it is a syntactically valid urn
    else {
        return [potentialProtocol, rest];
    }
}

function parseAuthentication(urlString) {
    // If there is no //, then we don't have authentication
    // If '//' didn't come first, then we're not looking at an authentication and we should bail
    if(!startsWith(urlString, '//')) {
        return [null, urlString];
    }

    let targetPart = removeStart(urlString, '//');
    let [authenticationPart, rest] = splitFirst(targetPart, '@');

    // If there is no @, then we don't have an authentication
    if(!isPresent(rest)) {
        return [null, '//' + targetPart];
    }

    let authenticationResults = splitFirst(authenticationPart, ':');

    // If there isn't a colon, then there is no password
    if(authenticationResults.length === 1) {
        return [{principal: first(authenticationResults), password: null}, '//' + rest];
    }

    let [principal, password] = authenticationResults;

    // The authentication section started with a :, don't know what to make of this... password but no username?
    // just parse out the authentication section and soldier on with no authentication information
    if(isEmpty(principal)) {
        return [null, '//' + rest];
    }

    // Username with no password - this is legit
    if(isEmpty(password)) {
        return [{principal, password: null}, '//' + rest];
    }

    // Otherwise, we have both, so return the complete authentication object and the rest
    return [{principal, password: emptyToNull(password)}, '//' + rest];
}

function parseHost(urlString, parseOptions) {
    // Check if the host is starting with reserved characters, if so we should just bail on trying to parse
    if(startsWith(urlString, '?') || startsWith(urlString, '#')) {
        return [null, urlString];
    }

    let hostRequired = startsWith(urlString, '//');
    if(!hostRequired && !parseOptions.opaque) {
        return [null, urlString];
    }

    urlString = removeStart(urlString, '//');

    // Lets grab everything to the left of the first slash, this is the remainder of our authority (if any)
    let [authority, rest] = splitFirst(urlString, '/');
    if(isPresent(rest)) {
        rest = '/' + rest;
    }

    // If the rest of the url starts with a slash, then we'll assume we don't have an host and it's just starting with a path
    if(isEmpty(authority)) {
        return [null, urlString];
    }

    let hostResults = splitFirst(authority, ':');

    // We have no :, which means no port, but might have a hostname
    if(hostResults.length === 1) {
        // Did we just start with a : but not have any actual values? If so, just ditch having a host
        if(isEmpty(hostResults[0])) {
            return [null, rest];
        }

        return [{name: hostResults[0], domain: parseDomain(hostResults[0]), port: null}, rest];
    }

    let [name, port] = hostResults;

    // The host started with a :, this is odd, lets just disregard
    if(isEmpty(name)) {
        return [null, rest];
    }

    // Otherwise, we have both, so return the complete authentication object and the rest
    return [{name, domain: parseDomain(name), port: emptyToNull(port)}, rest];
}

function parseDomain(domainName) {
    return reverse(split(domainName, '.'));
}

function parseAuthority(urlString, parseOptions) {
    let [authentication, rest1] = parseAuthentication(urlString, parseOptions);
    let [host, rest2] = parseHost(rest1, parseOptions);

    if(isNil(authentication) && isNil((host))) {
        return [null, rest2];
    }

    return [{host, authentication}, rest2];
}

function parseLocation(urlString, parseOptions, context) {
    let location = {};
    location.path = [];
    location.parameters = {};
    location.relative = !isEmpty(urlString) && !startsWith(urlString, '/') && !isPresent(context.protocol) && !isPresent(context.authority);

    let urlPartition = partitionLocationInformation(urlString);

    location.rawPath = urlPartition.path;

    each(removeStart(urlPartition.path, '/').split('/'), function(urlPathPart) {
        if(!isBlank(urlPathPart) || !parseOptions.collapseEmptyPathSegments) {
            location.path.push(decode(urlPathPart));
        }
    });

    location.query = urlPartition.query;

    each(defaultString(urlPartition.query).split('&'), function(parameterPair) {
        let splitParameters = parameterPair.split('=');

        if(!isBlank(first(splitParameters))) {
            let key = decodeRequestParameter(splitParameters[0]);
            let value = '';
            if(splitParameters.length === 2) {
                value = splitParameters[1];
            }
            if(isNil(location.parameters[key])) {
                location.parameters[key] = decodeRequestParameter(value);
            }
            else if(!isArray(location.parameters[key])) {
                let paramList = [location.parameters[key]];
                paramList.push(decodeRequestParameter(value));
                location.parameters[key] = paramList;
            }
            else {
                location.parameters[key].push(decodeRequestParameter(value));
            }
        }
    });

    location.fragment = urlPartition.fragment;
    return location;
}

export function partitionLocationInformation(urlString) {
    let partition = {path: null, query: null, fragment: null};
    let target = defaultString(urlString);

    // First, lets see if we have a fragment and parse it off the end
    let results = splitLast(target, '#');

    if(results.length === 2) {
        let [rest, fragment] = results;
        partition.fragment = fragment;
        target = rest;
    }

    // Then lets see if we have a query string and parse it off the remainder
    results = splitLast(target, '?');

    if(results.length === 2) {
        let [rest, query] = results;
        partition.query = query;
        target = rest;
    }

    partition.path = target;

    return partition;
}

const DEFAULT_PARSE_OPTIONS = {
    opaque: false, // controls whether or not we support 'opaque' parsing - ie allowing some url formats which deviate from the spec
    collapseEmptyPathSegments: true, // controls whether or not we allow empty path segments in the parsed structure
};

const DEFAULT_URI_PARSE_OPTIONS = DEFAULT_PARSE_OPTIONS;

const DEFAULT_URL_PARSE_OPTIONS = defaults({opaque: true}, DEFAULT_PARSE_OPTIONS);

const DEFAULT_HREF_PARSE_OPTIONS = defaults({}, DEFAULT_PARSE_OPTIONS);

export function parseUri(urlString, parseOptions = DEFAULT_URI_PARSE_OPTIONS) {
    if(!isString(urlString)) {
        return urlString;
    }

    let context = {};
    let [protocol, rest1] = parseProtocol(urlString, parseOptions, context);
    context.protocol = protocol;

    let [authority, rest2] = parseAuthority(rest1, parseOptions, context);
    context.authority = authority;

    let location = parseLocation(rest2, parseOptions, context);
    return constructUrl(urlString, protocol, authority, location);
}

export function parseUrl(urlString, parseOptions = DEFAULT_URL_PARSE_OPTIONS) {
    return parseUri(urlString, parseOptions);
}

export function parseHref(urlString, parseOptions = DEFAULT_HREF_PARSE_OPTIONS) {
    return parseUri(urlString, parseOptions);
}

export function resolveHref(baseUrl, targetUrl) {
    if(!isObject(targetUrl)) {
        targetUrl = parseHref(targetUrl);
    }

    if(isPresent(targetUrl.authority)) {
        return targetUrl;
    }

    if(!isObject(baseUrl)) {
        baseUrl = parseUrl(baseUrl);
    }

    let resolvedUrl = produce(baseUrl, draft => {
        draft.location.query = targetUrl.location.query;
        draft.location.parameters = targetUrl.location.parameters;
        draft.location.fragment = targetUrl.location.fragment;

        if(isEmpty(draft.location.path) || !targetUrl.location.relative) {
            draft.location.path = targetUrl.location.path;
        }
        else if(!isEmpty(targetUrl.location.path)) {
            draft.location.path = [...take(draft.location.path, draft.location.path.length - 1), ...targetUrl.location.path];
        }

        draft.rawUrl = formatUrl(draft);
        draft.location.rawPath = formatLocation(draft.location);
    });

    return resolvedUrl;
}

export function formatUrl(url, options = {excludeComponents: []}) {
    let { excludeComponents } = options;

    let urlString = '';
    if(isPresent(url.protocol) && !contains(excludeComponents, uriComponents.PROTOCOL)) {
        urlString = urlString + url.protocol;
    }

    if(isPresent(url.authority) && !contains(excludeComponents, uriComponents.AUTHORITY)) {
        if(isPresent(url.protocol)) {
            urlString = urlString + '://';
        }

        if(isPresent(url.authority.authentication)) {
            urlString = urlString + url.authority.authentication.principal;

            if(isPresent(url.authority.authentication.password)) {
                urlString = urlString + ':' + url.authority.authentication.password;
            }

            urlString = urlString + '@';
        }

        urlString = urlString + url.authority.host.name;

        if(isPresent(url.authority.host.port)) {
            urlString = urlString + ':' + url.authority.host.port;
        }
    }

    if(!contains(excludeComponents, uriComponents.LOCATION)) {
        const locationString = formatLocation(url.location, options);

        if(locationString !== '/' || isEmpty(urlString)) {
            urlString = urlString + locationString;
        }
    }

    return urlString;
}

export function formatLocation(location, options = {excludeComponents: []}) {
    let urlString = '/';
    let { excludeComponents } = options;

    const includePath = !isEmpty(location.path);
    const includeParameters = !isEmpty(location.query) && !contains(excludeComponents, uriComponents.PARAMETERS);
    const includeFragment = !isBlank(location.fragment) && !contains(excludeComponents, uriComponents.FRAGMENT);

    if(includePath) {
        let path = join(map(location.path, pathPart => encode(pathPart)), '/');
        urlString = urlString + path;
    }

    if(includeParameters) {
        urlString = urlString + '?' + location.query;
    }

    if(includeFragment) {
        urlString = urlString + '#' + encode(location.fragment);
    }

    return urlString;
}

export function empty() {
    return constructUrl();
}