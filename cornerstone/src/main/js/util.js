import {
    compact,
    first,
    forEach,
    includes,
    isEmpty,
    isFunction,
    isNil,
    isNull,
    isUndefined,
    toInteger,
    take,
    last,
    remove,
    isArray,
    isString,
    split as splitLodash
} from 'lodash-es';

export const DIGITS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

export const defaultList = val => {
    if(isNil(val)) {
        return [];
    }

    return val;
};

export const defaultMap = val => {
    if(isNil(val)) {
        return {};
    }

    return val;
};

export const isPresent = obj => {
    return !isNull(obj) && !isUndefined(obj);
};

export const isBlank = str => {
    if (str === null || str === undefined) str = '';
    return (/^\s*$/).test(str);
};

export const valueAt = (array, index) => {
    if(!isPresent(array) || !isPresent(index)) {
        return null;
    }
    if(!(array.length > index)) {
        return null;
    }

    return array[index];
};

export const defaultString = (string, defaultString = '') => {
    if(!isPresent(string)) {
        return defaultString;
    }

    return string;
};

export const valuate = (element, ...args) => {
    if(isFunction(element)) {
        return element(...args);
    }

    return element;
};

export const buildPromise = () => {
    let resolveVar = null;
    let rejectVar = null;
    let promise = new Promise(function (resolve, reject) {
        resolveVar = resolve;
        rejectVar = reject;
    });

    return [promise, resolveVar, rejectVar];
};

export const conformInteger = (val) => {
    if(isBlank(val)) {
        return null;
    }
    return toInteger(val);
};

export const bound = (val, lower, upper) => {
    if(isPresent(lower) && val < lower) {
        return lower;
    }

    if(isPresent(upper) && val > upper) {
        return upper;
    }

    return val;
};

export const valuesOrdered = (obj, paths) => {
    let result = [];
    forEach(paths, path => result.push(obj[path]));
    return compact(result);
};

export const abs = (value) => Math.abs(value);

export const replaceAt = (string, index, replace) => {
    return string.substring(0, index) + replace + string.substring(index + 1);
};

export const split = (string, splitString) => {
    if(!isPresent(string)) {
        return [];
    }

    return splitLodash(string, splitString);
};

export const splitFirst = (string, splitString) => {
    let results = split(string, splitString);
    if(results.length === 1) {
        return results;
    }

    return [first(results), rest(results).join(splitString)];
};

export const splitLast = (string, splitString) => {
    let results = split(string, splitString);
    if(results.length === 1) {
        return results;
    }

    return [take(results, results.length - 1).join(splitString), last(results)];
};

export const removeStart = (string, substring) => {
    if (string.indexOf(substring) !== 0) {
        return string;
    }

    return string.slice(substring.length);
};

export const removeFirst = (string, substring) => replaceFirst(string, substring, '');

export const removeAll = (collection, values) => {
    if(isArray(collection)) {
        return remove(collection, element => contains(values, element));
    }
    if(isString(collection)) {
        for(const string of values) {
            collection = replaceAll(collection, string, '');
        }

        return collection;
    }
};

export const replaceFirst = (string, substring, replacement) => {
    if(isNil(string)) {
        return string;
    }

    return string.replace(substring, replacement);
};

export const escapeRegex = (string) => {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&')
};

export const replaceAll = (string, substring, replacement) => {
    if(isNil(string)) {
        return string;
    }

    const regex = escapeRegex(substring);
    return string.replace(new RegExp(regex, 'g'), replacement);
};

export const rest = (array) => {
    if(array.length <= 1) {
        return [];
    }

    // eslint-disable-next-line no-unused-vars
    let [first, ...rest] = array;
    return rest;
};

export const emptyToNull = (string) => {
    if(isEmpty(string)) {
        return null;
    }

    return string;
};

export const randomUuid = () => {
    // For older browsers use the less robust Math.random()
    if (typeof crypto === 'undefined') {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
};

export const contains = includes;

export {
    add,
    after,
    ary,
    assign,
    assignIn,
    assignInWith,
    assignWith,
    at,
    attempt,
    before,
    bind,
    bindAll,
    bindKey,
    camelCase,
    capitalize,
    castArray,
    ceil,
    chain,
    chunk,
    clamp,
    clone,
    cloneDeep,
    cloneDeepWith,
    cloneWith,
    commit,
    compact,
    concat,
    cond,
    conforms,
    conformsTo,
    constant,
    countBy,
    create,
    curry,
    curryRight,
    debounce,
    deburr,
    defaultTo,
    defaults,
    defaultsDeep,
    defer,
    delay,
    difference,
    differenceBy,
    differenceWith,
    divide,
    drop,
    dropRight,
    dropRightWhile,
    dropWhile,
    each,
    eachRight,
    endsWith,
    entries,
    entriesIn,
    eq,
    escape,
    escapeRegExp,
    every,
    extend,
    extendWith,
    fill,
    filter,
    find,
    findIndex,
    findKey,
    findLast,
    findLastIndex,
    findLastKey,
    first,
    flatMap,
    flatMapDeep,
    flatMapDepth,
    flatten,
    flattenDeep,
    flattenDepth,
    flip,
    floor,
    flow,
    flowRight,
    forEach,
    forEachRight,
    forIn,
    forInRight,
    forOwn,
    forOwnRight,
    fromPairs,
    functions,
    functionsIn,
    get,
    groupBy,
    gt,
    gte,
    has,
    hasIn,
    head,
    identity,
    inRange,
    includes,
    indexOf,
    initial,
    intersection,
    intersectionBy,
    intersectionWith,
    invert,
    invertBy,
    invoke,
    invokeMap,
    isArguments,
    isArray,
    isArrayBuffer,
    isArrayLike,
    isArrayLikeObject,
    isBoolean,
    isBuffer,
    isDate,
    isElement,
    isEmpty,
    isEqual,
    isEqualWith,
    isError,
    isFinite,
    isFunction,
    isInteger,
    isLength,
    isMap,
    isMatch,
    isMatchWith,
    isNaN,
    isNative,
    isNil,
    isNull,
    isNumber,
    isObject,
    isObjectLike,
    isPlainObject,
    isRegExp,
    isSafeInteger,
    isSet,
    isString,
    isSymbol,
    isTypedArray,
    isUndefined,
    isWeakMap,
    isWeakSet,
    iteratee,
    join,
    kebabCase,
    keyBy,
    keys,
    keysIn,
    last,
    lastIndexOf,
    lowerCase,
    lowerFirst,
    lt,
    lte,
    map,
    mapKeys,
    mapValues,
    matches,
    matchesProperty,
    max,
    maxBy,
    mean,
    meanBy,
    memoize,
    merge,
    mergeWith,
    method,
    methodOf,
    min,
    minBy,
    multiply,
    negate,
    next,
    noop,
    now,
    nth,
    nthArg,
    omit,
    omitBy,
    once,
    orderBy,
    over,
    overArgs,
    overEvery,
    overSome,
    pad,
    padEnd,
    padStart,
    parseInt,
    partial,
    partialRight,
    partition,
    pick,
    pickBy,
    plant,
    property,
    propertyOf,
    pull,
    pullAll,
    pullAllBy,
    pullAllWith,
    pullAt,
    random,
    range,
    rangeRight,
    rearg,
    reduce,
    reduceRight,
    reject,
    remove,
    repeat,
    replace,
    result,
    reverse,
    round,
    sample,
    sampleSize,
    set,
    setWith,
    shuffle,
    size,
    slice,
    snakeCase,
    some,
    sortBy,
    sortedIndex,
    sortedIndexBy,
    sortedIndexOf,
    sortedLastIndex,
    sortedLastIndexBy,
    sortedLastIndexOf,
    sortedUniq,
    sortedUniqBy,
    spread,
    startCase,
    startsWith,
    stubArray,
    stubFalse,
    stubObject,
    stubString,
    stubTrue,
    subtract,
    sum,
    sumBy,
    tail,
    take,
    takeRight,
    takeRightWhile,
    takeWhile,
    tap,
    template,
    templateSettings,
    throttle,
    thru,
    times,
    toArray,
    toFinite,
    toInteger,
    toIterator,
    toJSON,
    toLength,
    toLower,
    toNumber,
    toPairs,
    toPairsIn,
    toPath,
    toPlainObject,
    toSafeInteger,
    toString,
    toUpper,
    transform,
    trim,
    trimEnd,
    trimStart,
    truncate,
    unary,
    unescape,
    union,
    unionBy,
    unionWith,
    uniq,
    uniqBy,
    uniqWith,
    uniqueId,
    unset,
    unzip,
    unzipWith,
    update,
    updateWith,
    upperCase,
    upperFirst,
    value,
    valueOf,
    values,
    valuesIn,
    without,
    words,
    wrap,
    wrapperAt,
    wrapperChain,
    wrapperCommit,
    wrapperLodash,
    wrapperNext,
    wrapperPlant,
    wrapperReverse,
    wrapperToIterator,
    wrapperValue,
    xor,
    xorBy,
    xorWith,
    zip,
    zipObject,
    zipObjectDeep,
    zipWith
} from 'lodash-es';