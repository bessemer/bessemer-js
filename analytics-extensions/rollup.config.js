import replace from '@rollup/plugin-replace'
import json from '@rollup/plugin-json'
import babel from 'rollup-plugin-babel'
import multiInput from 'rollup-plugin-multi-input';
import generatePackageJson from 'rollup-plugin-generate-package-json'
import pkg from './package.json'

const makeExternalPredicate = externalArr => {
    if (externalArr.length === 0) {
        return () => false;
    }

    const pattern = new RegExp(`^(${externalArr.join('|')})($|/)`);
    return id => pattern.test(id);
};

const config = {
    input: ['src/main/js/*.js'],
    output: {
        name: pkg.name,
        exports: 'named',
        dir: 'build',
        format: 'es'
    },
    external: makeExternalPredicate(
        [
            ...Object.keys(pkg.dependencies || {}),
            ...Object.keys(pkg.peerDependencies || {})
        ]
    ),
    plugins: [
        // generatePackageJson({
        //     baseContents: (pkg) => ({
        //         ...pkg,
        //         scripts: null,
        //         exports: null
        //     })
        // }),
        multiInput({relative: 'src/main/js/'}),
        json(),
        babel({
            exclude: 'node_modules/**',
            babelrc: false,
            runtimeHelpers: true,
            presets: [
                ["@babel/env", {
                    modules: false,
                    loose: true,
                    targets: {
                        browsers: ["> 1%", "last 2 versions"]
                    }
                }],
                '@babel/preset-flow'
            ],
            plugins: [
                ['@babel/plugin-transform-runtime', {
                    useESModules: true
                }],
                '@babel/plugin-syntax-import-meta',
                '@babel/plugin-proposal-class-properties',
                ['@babel/plugin-proposal-decorators', {
                    decoratorsBeforeExport: true
                }],
                '@babel/plugin-proposal-export-namespace-from',
                '@babel/plugin-proposal-numeric-separator',
                ['babel-plugin-root-import', {
                    paths: [{
                        rootPathSuffix: './src/main/js',
                        rootPathPrefix: '~/'
                    }, {
                        rootPathSuffix: './src',
                        rootPathPrefix: '!/'
                    }],
                }]
            ]
        }),
        replace({
            'process.env.NODE_ENV': JSON.stringify(
                'development'
            )
        })
    ].filter(Boolean)
};

export default config;