const path = require('path');

let build = path.resolve(__dirname, 'build');

module.exports = {
	entry: {
		main: './src/main/js/index.js',
	},
	output: {
		path: path.resolve(build),
		filename: 'index.js',
		libraryTarget: 'commonjs2',
	},
	module: {
		rules: [
			{
				test: /\.js?$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
				},
			},
		],
	},
	optimization: {
		minimize: false
	},
	externals : {
		'flatten-anything': 'flatten-anything',
		'immer': 'immer'
	}
};