import produce from 'immer';

const defaultConfiguration = {
    identifyNamespace: null,
    pageNamespace: null,
    debug: false,
};

const pluginName = 'namespace';
const loggerPrefix = `[analytics/${pluginName}]`;

export class AnalyticsNamespacePlugin {
    constructor(additionalConfiguration = {}) {
        this.name = pluginName;
        this.config = { ...defaultConfiguration, ...additionalConfiguration };
    }

    identifyStart = ({payload}) => {
        this.log('identifyStart', payload);

        if(this.config.identifyNamespace) {
            return produce(payload, draft => {
                let namespace = {};
                namespace[this.config.identifyNamespace] = payload.traits;

                draft.traits = namespace;
            });
        }
        else {
            return payload;
        }
    };

    pageStart = ({payload}) => {
        this.log('pageStart', payload);

        if(this.config.pageNamespace) {
            return produce(payload, draft => {
                let namespace = {};
                namespace[this.config.pageNamespace] = payload.properties;

                draft.properties = namespace;
            });
        }
        else {
            return payload;
        }
    };

    log = (message, params) => {
        if(!this.config.debug) {
            return;
        }

        if(params === undefined) {
            console.log(`${loggerPrefix} ${message}`);
        }
        else {
            console.log(`${loggerPrefix} ${message}`, params);
        }
    };
}