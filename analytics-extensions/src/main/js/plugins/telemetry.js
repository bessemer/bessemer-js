import produce from 'immer';
import { fromPairs } from '@bessemer/cornerstone/util';

const defaultConfiguration = {
    telemetryStore: simpleTelemetryStore(),
    extractTelemetry: null,
    debug: false,
};

function simpleTelemetryStore() {
    let currentTelemetry = {};

    let setTelemetry = (additionalTelemetry) => {
        currentTelemetry = {...currentTelemetry, ...additionalTelemetry}
    };

    return () => {
        return [currentTelemetry, setTelemetry]
    };
}
const pluginName = 'telemetry';
const loggerPrefix = `[analytics/${pluginName}]`;

export class AnalyticsTelemetryPlugin {
    static selectTelemetry = (targets) => {
        return (payload) => {
            return fromPairs(Object.entries(getPayloadData(payload)).filter(([key, _]) => targets.includes(key)));
        };
    };

    constructor(additionalConfiguration = {}) {
        this.name = pluginName;
        this.config = { ...defaultConfiguration, ...additionalConfiguration };

        const {telemetryStore, extractTelemetry} = this.config;

        if(!telemetryStore) {
            throw new Error(`${loggerPrefix} initialize - telemetryStore must be defined`);
        }
        if(!extractTelemetry) {
            throw new Error(`${loggerPrefix} initialize - extractTelemetry must be defined`);
        }
    }

    applyTelemetry = (payload) => {
        let [currentTelemetry, setTelemetry] = this.config.telemetryStore();

        let additionalTelemetry = this.config.extractTelemetry(payload);
        this.log('attaching telemetry', additionalTelemetry);

        let telemetry = {...currentTelemetry, ...additionalTelemetry};
        setTelemetry(additionalTelemetry);

        return setPayloadData(payload, telemetry);
    };

    identifyStart = ({payload}) => {
        this.log('identifyStart', payload);
        return this.applyTelemetry(payload, 'traits');
    };

    pageStart = ({payload, instance}) => {
        this.log('pageStart', payload);
        return this.applyTelemetry(payload);
    };

    trackStart = ({payload, instance}) => {
        this.log('trackStart', payload);
        return this.applyTelemetry(payload);
    };

    log = (message, params) => {
        if(!this.config.debug) {
            return;
        }

        if(params === undefined) {
            console.log(`${loggerPrefix} ${message}`);
        }
        else {
            console.log(`${loggerPrefix} ${message}`, params);
        }
    };
}

function getPayloadData(payload) {
    if(payload.traits) {
        return payload.traits;
    }
    else if(payload.properties) {
        return payload.properties;
    }
    else {
        return {};
    }
}

function setPayloadData(payload, data) {
    return produce(payload, draft => {
        if(payload.traits) {
            draft.traits = {...data, ...payload.traits};
        }
        else if(payload.properties) {
            draft.properties = {...data, ...payload.properties};
        }
    });
}
