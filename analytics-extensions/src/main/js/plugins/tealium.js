import flatten from 'flatten-anything'

const defaultConfiguration = {
    account: null,
    profile: null,
    environment: 'dev',
    debug: false,
};

const pluginName = 'tealium';
const loggerPrefix = `[analytics/${pluginName}]`;

export class AnalyticsTealiumPlugin {
    constructor(additionalConfiguration = {}) {
        this.name = pluginName;
        this.config = {...defaultConfiguration, ...additionalConfiguration};

        const {account, profile, environment} = this.config;

        if (!account) {
            throw new Error(`${loggerPrefix} initialize - account must be defined`);
        }
        if (!profile) {
            throw new Error(`${loggerPrefix} initialize - profile must be defined`);
        }
        if (!environment) {
            throw new Error(`${loggerPrefix} initialize - environment must be defined`);
        }
    }

    bootstrap = (params) => {
        this.log('bootstrap', params);

        // Disable automatic page tracking and allow the app to handle it manually
        window.utag_cfg_ovrd = window.utag_cfg_ovrd || {};
        window.utag_cfg_ovrd.noview = true;
    };

    initialize = ({config}) => {
        this.log('initialize', config);

        if (!window.utag_data) {
            window.utag_data = {};
        }

        const {account, profile, environment} = config;

        if (!isTealiumLoaded()) {
            // Load the tealium tag manager
            (function (a, b, c, d) {
                a = `https://tags.tiqcdn.com/utag/${account}/${profile}/${environment}/utag.js`;
                b = document;
                c = 'script';
                d = b.createElement(c);
                d.src = a;
                d.type = 'text/java' + c;
                d.async = true;
                a = b.getElementsByTagName(c)[0];
                a.parentNode.insertBefore(d, a);
            })();
        }
    };

    loaded = () => {
        return window.utag_data && window.utag && isTealiumLoaded();
    };

    identify = ({payload, instance}) => {
        this.log('identify', payload);

        window.utag.link(flatten({
            tealium_event: 'identify',
            ...payload.traits,
        }));
    };

    page = ({payload, instance}) => {
        this.log('page', payload);

        window.utag.view(flatten(payload.properties));
    };

    track = ({payload, instance}) => {
        this.log('track', payload);

        window.utag.link(flatten({
            tealium_event: payload.event,
            ...payload.properties,
        }));
    };

    log = (message, params) => {
        if (!this.config.debug) {
            return;
        }

        if (params === undefined) {
            console.log(`${loggerPrefix} ${message}`);
        } else {
            console.log(`${loggerPrefix} ${message}`, params);
        }
    };
}

function isTealiumLoaded() {
    const scripts = Array.from(document.getElementsByTagName('script'));
    return scripts.filter(({src}) => src.match(/tags\.tiqcdn\.com/g)).length > 0
}
