const _ = require('lodash');
const path = require('path');

const StyleguideMaterialModule = require('@bessemer/styleguide-material/build/module');

var name = 'bessemer-commerce-material';
exports.name = name;

var Styles = {};

Styles.paths = _.concat(path.resolve(__dirname, '../styles'), StyleguideMaterialModule.Styles.paths);

exports.Styles = Styles;

var Storybook = {};

Storybook.paths = _.concat(path.resolve(__dirname, '../stories'), StyleguideMaterialModule.Storybook.paths);

Storybook.loadStories = function() {
	StyleguideMaterialModule.Storybook.loadStories();
	require('../stories');
};

exports.Storybook = Storybook;