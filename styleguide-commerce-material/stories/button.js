import React from 'react';
import { storiesOf } from '@storybook/react';

import Button from '@material-ui/core/Button';

let story = storiesOf('Commerce Buttons', module);

story.add('Commerce Buttons', () => {
	return (
		<Button variant="contained" color="primary">
			Hello Commerce
		</Button>
	);
});