import { configure, addDecorator } from '@storybook/react';
import { withNotes } from '@storybook/addon-notes';

import * as MainModule from '../build/module';

function loadStories() {
	MainModule.Storybook.loadStories();
}

// const BootstrapDecorator = (storyFn) => (
// 	<div className="container-fluid mt-3">
// 		<div className="row">
// 			<div className="col">
// 				{ storyFn() }
// 			</div>
// 		</div>
// 	</div>
// );
//
// addDecorator(BootstrapDecorator);

addDecorator(withNotes);
configure(loadStories, module);