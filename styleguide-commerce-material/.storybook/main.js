const _ = require('lodash');
const path = require('path');
const MainModule = require('../build/module');

console.log('=== Resolved Module ===', MainModule);

module.exports = {
    addons: [
        '@storybook/addon-storysource/register',
        '@storybook/addon-notes/register'
    ],
    webpackFinal: (config) => {
        console.log(config);

        config.module = {
            rules: [
                {
                    test: /\.js?$/,
                    include: _.concat(path.resolve(__dirname, '../'), MainModule.Storybook.paths),
                    use:
                        [
                            {
                                loader: 'babel-loader',
                                options: {
                                    presets:
                                        [
                                            ["@babel/env", {
                                                "targets": {
                                                    "browsers": ["> 1%", "last 2 versions"]
                                                }
                                            }],
                                            "@babel/preset-react"
                                        ],
                                    "plugins": [
                                        "@babel/plugin-proposal-class-properties",
                                        "@babel/plugin-proposal-optional-chaining"
                                    ]
                                }
                            },
                            {
                                loader: '@storybook/addon-storysource/loader'
                            },
                        ],
                },
                {
                    test: /\.scss$/,
                    // TODO we need to find a way to fix these hardcoded paths
                    include: _.concat(path.resolve(__dirname, '../'), MainModule.Styles.paths),
                    use: [
                        {loader: 'style-loader'},
                        {loader: 'css-loader'},
                        {loader: 'sass-loader'},
                    ]
                }
            ],
        };

        return config;
    },
};