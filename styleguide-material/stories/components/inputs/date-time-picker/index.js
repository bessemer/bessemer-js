import React, {useState} from 'react';
import {storiesOf} from '@storybook/react';

import {Grid} from '@material-ui/core';
import { DatePicker, KeyboardDatePicker } from "@material-ui/pickers";

let story = storiesOf('Components/Inputs/Date Time Picker', module);

const BasicDatePicker = () => {
    const [selectedDate, handleDateChange] = useState(new Date());

    return (
        <Grid container spacing={3}>
            <Grid item>
                <DatePicker
                    label="Basic example"
                    value={selectedDate}
                    onChange={handleDateChange}
                    animateYearScrolling
                />
            </Grid>
            <Grid item>
                <DatePicker
                    autoOk
                    label="Clearable"
                    clearable
                    disableFuture
                    value={selectedDate}
                    onChange={handleDateChange}
                />
            </Grid>
            <Grid item>
                <DatePicker
                    disableFuture
                    openTo="year"
                    format="dd/MM/yyyy"
                    label="Date of birth"
                    views={["year", "month", "date"]}
                    value={selectedDate}
                    onChange={handleDateChange}
                />
            </Grid>
        </Grid>
    );
};

story.add('Date Picker', () => {
    return (
        <BasicDatePicker />
    );
});

const KeyboardDatePickerExample = () => {
    const [selectedDate, handleDateChange] = useState(new Date());

    return (
        <Grid container spacing={3}>
            <Grid item>
                <KeyboardDatePicker
                    clearable
                    value={selectedDate}
                    placeholder="10/10/2018"
                    onChange={date => handleDateChange(date)}
                    minDate={new Date()}
                    format="MM/dd/yyyy"
                />
            </Grid>
            <Grid item>
                <KeyboardDatePicker
                    placeholder="2018/10/10"
                    value={selectedDate}
                    onChange={date => handleDateChange(date)}
                    format="yyyy/MM/dd"
                />
            </Grid>
        </Grid>
    );
};

story.add('Keyboard Input', () => {
    return (
        <KeyboardDatePickerExample />
    );
});

const YearMonthPicker = () => {
    const [selectedDate, handleDateChange] = useState(new Date());

    return (
        <Grid container spacing={3}>
            <Grid item>
                <DatePicker
                    views={["year"]}
                    label="Year only"
                    value={selectedDate}
                    onChange={handleDateChange}
                />
            </Grid>
            <Grid item>
                <DatePicker
                    views={["year", "month"]}
                    label="Year and Month"
                    helperText="With min and max"
                    minDate={new Date("2018-03-01")}
                    maxDate={new Date("2018-06-01")}
                    value={selectedDate}
                    onChange={handleDateChange}
                />
            </Grid>
            <Grid item>
                <DatePicker
                    variant="inline"
                    openTo="year"
                    views={["year", "month"]}
                    label="Year and Month"
                    helperText="Start from year selection"
                    value={selectedDate}
                    onChange={handleDateChange}
                />
            </Grid>
        </Grid>
    );
};

story.add('Different Views', () => {
    return (
        <YearMonthPicker />
    );
});

const InlineDatePicker = () => {
    const [selectedDate, handleDateChange] = useState(new Date());

    return (
        <Grid container spacing={3}>
            <Grid item>
                <DatePicker
                    variant="inline"
                    label="Basic example"
                    value={selectedDate}
                    onChange={handleDateChange}
                />
            </Grid>
            <Grid item>
                <DatePicker
                    disableToolbar
                    variant="inline"
                    label="Only calendar"
                    helperText="No year selection"
                    value={selectedDate}
                    onChange={handleDateChange}
                />
            </Grid>
            <Grid item>
                <KeyboardDatePicker
                    autoOk
                    variant="inline"
                    inputVariant="outlined"
                    label="With keyboard"
                    format="MM/dd/yyyy"
                    value={selectedDate}
                    InputAdornmentProps={{ position: "start" }}
                    onChange={date => handleDateChange(date)}
                />
            </Grid>
        </Grid>
    );
};

story.add('Inline Mode', () => {
    return (
        <InlineDatePicker />
    );
});

const StaticDatePicker = () => {
    const [date, changeDate] = useState(new Date());

    return (
        <Grid container spacing={3}>
            <Grid item>
                <DatePicker
                    autoOk
                    variant="static"
                    openTo="year"
                    value={date}
                    onChange={changeDate}
                />
            </Grid>
            <Grid item>
                <DatePicker
                    autoOk
                    orientation="landscape"
                    variant="static"
                    openTo="date"
                    value={date}
                    onChange={changeDate}
                />
            </Grid>
        </Grid>
    );
};

story.add('Static Mode', () => {
    return (
        <StaticDatePicker />
    );
});