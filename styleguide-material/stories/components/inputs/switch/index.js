import './basic-switches'
import './labeled-switches'
import './form-group-switches'
import './customized-switches'
import './sizes'
import './label-placement'