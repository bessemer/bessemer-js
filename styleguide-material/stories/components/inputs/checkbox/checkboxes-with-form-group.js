import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {Checkbox, FormControl, FormControlLabel, FormGroup, FormHelperText, FormLabel} from '@material-ui/core';
import {storiesOf} from '@storybook/react';

let story = storiesOf('Components/Inputs/Checkbox', module);


const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    formControl: {
        margin: theme.spacing(3),
    },
}));

const CheckboxesGroup = () => {
    const classes = useStyles();
    const [state, setState] = React.useState({
        gilad: true,
        jason: false,
        antoine: false,
    });

    const handleChange = name => event => {
        setState({ ...state, [name]: event.target.checked });
    };

    const { gilad, jason, antoine } = state;
    const error = [gilad, jason, antoine].filter(v => v).length !== 2;

    return (
        <div className={classes.root}>
            <FormControl component="fieldset" className={classes.formControl}>
                <FormLabel component="legend">Assign responsibility</FormLabel>
                <FormGroup>
                    <FormControlLabel
                        control={<Checkbox checked={gilad} onChange={handleChange('gilad')} value="gilad" />}
                        label="Gilad Gray"
                    />
                    <FormControlLabel
                        control={<Checkbox checked={jason} onChange={handleChange('jason')} value="jason" />}
                        label="Jason Killian"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox checked={antoine} onChange={handleChange('antoine')} value="antoine" />
                        }
                        label="Antoine Llorca"
                    />
                </FormGroup>
                <FormHelperText>Be careful</FormHelperText>
            </FormControl>
            <FormControl required error={error} component="fieldset" className={classes.formControl}>
                <FormLabel component="legend">Pick two</FormLabel>
                <FormGroup>
                    <FormControlLabel
                        control={<Checkbox checked={gilad} onChange={handleChange('gilad')} value="gilad" />}
                        label="Gilad Gray"
                    />
                    <FormControlLabel
                        control={<Checkbox checked={jason} onChange={handleChange('jason')} value="jason" />}
                        label="Jason Killian"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox checked={antoine} onChange={handleChange('antoine')} value="antoine" />
                        }
                        label="Antoine Llorca"
                    />
                </FormGroup>
                <FormHelperText>You can display an error</FormHelperText>
            </FormControl>
        </div>
    );
};

story.add('Checkboxes with Form Group', () => {
    return (
        <CheckboxesGroup />
    );
});