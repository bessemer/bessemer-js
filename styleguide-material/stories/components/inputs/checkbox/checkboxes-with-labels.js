import React from 'react';
import {Checkbox, FormControlLabel, FormGroup, withStyles} from '@material-ui/core';
import {green} from '@material-ui/core/colors';
import {CheckBox, CheckBoxOutlineBlank, Favorite, FavoriteBorder} from '@material-ui/icons';
import {storiesOf} from '@storybook/react';

let story = storiesOf('Components/Inputs/Checkbox', module);

const GreenCheckbox = withStyles({
    root: {
        color: green[400],
        '&$checked': {
            color: green[600],
        },
    },
    checked: {},
})(props => <Checkbox color="default" {...props} />);

story.add('Checkboxes with Labels', () => {
    return (
        <FormGroup row>
            <FormControlLabel
                control={
                    <Checkbox value="checkedA" />
                }
                label="Secondary"
            />
            <FormControlLabel
                control={
                    <Checkbox
                        value="checkedB"
                        color="primary"
                    />
                }
                label="Primary"
            />
            <FormControlLabel disabled control={<Checkbox value="checkedD" />} label="Disabled" />
            <FormControlLabel disabled control={<Checkbox checked value="checkedE" />} label="Disabled" />
            <FormControlLabel
                control={
                    <Checkbox
                        value="checkedF"
                        indeterminate
                    />
                }
                label="Indeterminate"
            />
            <FormControlLabel
                control={
                    <GreenCheckbox
                        value="checkedG"
                    />
                }
                label="Custom color"
            />
            <FormControlLabel
                control={<Checkbox icon={<FavoriteBorder />} checkedIcon={<Favorite />} value="checkedH" />}
                label="Custom icon"
            />
            <FormControlLabel
                control={
                    <Checkbox
                        icon={<CheckBoxOutlineBlank fontSize="small" />}
                        checkedIcon={<CheckBox fontSize="small" />}
                        value="checkedI"
                    />
                }
                label="Custom size"
            />
        </FormGroup>
    );
});