import './checkbox';
import './checkboxes-with-labels';
import './checkboxes-with-form-group';
import './label-placement';
import './customized-checkbox';