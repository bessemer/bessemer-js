import React from 'react';
import {storiesOf} from '@storybook/react';
import {Checkbox, FormControl, FormControlLabel, FormGroup, FormLabel} from '@material-ui/core';

let story = storiesOf('Components/Inputs/Checkbox', module);

story.add('Label Placement', () => {
    return (
        <FormControl component="fieldset">
            <FormLabel component="legend">Label Placement</FormLabel>
            <FormGroup aria-label="position" row>
                <FormControlLabel
                    value="top"
                    control={<Checkbox color="primary" />}
                    label="Top"
                    labelPlacement="top"
                />
                <FormControlLabel
                    value="start"
                    control={<Checkbox color="primary" />}
                    label="Start"
                    labelPlacement="start"
                />
                <FormControlLabel
                    value="bottom"
                    control={<Checkbox color="primary" />}
                    label="Bottom"
                    labelPlacement="bottom"
                />
                <FormControlLabel
                    value="end"
                    control={<Checkbox color="primary" />}
                    label="End"
                    labelPlacement="end"
                />
            </FormGroup>
        </FormControl>
    );
});
