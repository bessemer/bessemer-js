import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import {storiesOf} from '@storybook/react';

let story = storiesOf('Components/Inputs/Text Field', module);

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 200,
        },
    },
}));

function TextFieldSizes() {
    const classes = useStyles();

    return (
        <form className={classes.root} noValidate autoComplete="off">
            <div>
                <TextField label="Size" id="standard-size-small" defaultValue="Small" size="small" />
                <TextField label="Size" id="standard-size-normal" defaultValue="Normal" />
            </div>
            <div>
                <TextField
                    label="Size"
                    id="filled-size-small"
                    defaultValue="Small"
                    variant="filled"
                    size="small"
                />
                <TextField label="Size" id="filled-size-normal" defaultValue="Normal" variant="filled" />
            </div>
            <div>
                <TextField
                    label="Size"
                    id="outlined-size-small"
                    defaultValue="Small"
                    variant="outlined"
                    size="small"
                />
                <TextField
                    label="Size"
                    id="outlined-size-normal"
                    defaultValue="Normal"
                    variant="outlined"
                />
            </div>
        </form>
    );
}

story.add('Sizes', () => {
    return (
        <TextFieldSizes />
    );
});