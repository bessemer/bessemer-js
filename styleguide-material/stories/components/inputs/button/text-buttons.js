import React from 'react';
import {storiesOf} from '@storybook/react';
import {Button, Grid} from '@material-ui/core';

let story = storiesOf('Components/Inputs/Button', module);

story.add('Text Buttons', () => {
    return (
        <Grid container spacing={3}>
            <Grid item>
                <Button>
                    Default
                </Button>
            </Grid>
            <Grid item>
                <Button color="primary">
                    Primary
                </Button>
            </Grid>
            <Grid item>
                <Button color="secondary">
                    Secondary
                </Button>
            </Grid>
            <Grid item>
                <Button disabled>
                    Disabled
                </Button>
            </Grid>
            <Grid item>
                <Button href="#" color="primary">
                    Link
                </Button>
            </Grid>
        </Grid>
    );
});