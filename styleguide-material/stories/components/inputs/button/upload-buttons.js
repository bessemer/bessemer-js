import React from 'react';
import {storiesOf} from '@storybook/react';
import {makeStyles} from '@material-ui/core/styles';
import {Button, Grid, IconButton} from '@material-ui/core';
import {PhotoCamera} from '@material-ui/icons';

let story = storiesOf('Components/Inputs/Button', module);

const useStyles = makeStyles(theme => ({
    uploadInput: {
        display: 'none',
    },
}));

story.add('Upload Buttons', () => {
    const classes = useStyles();

    return (
        <Grid container spacing={3}>
            <Grid item>
                <input
                    accept="image/*"
                    className={classes.uploadInput}
                    id="contained-button-file"
                    multiple
                    type="file"
                />
                <label htmlFor="contained-button-file">
                    <Button variant="contained" color="primary" component="span">
                        Upload
                    </Button>
                </label>
            </Grid>
            <Grid item>
                <input
                    accept="image/*"
                    className={classes.uploadInput}
                    id="text-button-file"
                    multiple
                    type="file"
                />
                <label htmlFor="text-button-file">
                    <Button component="span">Upload</Button>
                </label>
            </Grid>
            <Grid item>
                <input
                    accept="image/*"
                    className={classes.uploadInput}
                    id="outlined-button-file"
                    multiple
                    type="file"
                />
                <label htmlFor="outlined-button-file">
                    <Button variant="outlined" component="span">
                        Upload
                    </Button>
                </label>
            </Grid>
            <Grid item>
                <input accept="image/*" className={classes.uploadInput} id="icon-button-file" type="file"/>
                <label htmlFor="icon-button-file">
                    <IconButton color="primary" aria-label="upload picture" component="span">
                        <PhotoCamera/>
                    </IconButton>
                </label>
            </Grid>
        </Grid>
    );
});