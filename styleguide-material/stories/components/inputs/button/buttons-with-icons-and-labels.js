import React from 'react';
import {storiesOf} from '@storybook/react';
import {Button, Grid} from '@material-ui/core';
import {CloudUpload, Delete, KeyboardVoice, Save, Send} from '@material-ui/icons';

let story = storiesOf('Components/Inputs/Button', module);

story.add('Buttons with Icons and Labels', () => {
    return (
        <Grid container spacing={3}>
            <Grid item>
                <Button
                    variant="contained"
                    color="secondary"
                    startIcon={<Delete/>}
                >
                    Delete
                </Button>
            </Grid>
            <Grid item>
                <Button
                    variant="contained"
                    color="primary"
                    endIcon={<Send/>}
                >
                    Send
                </Button>
            </Grid>
            <Grid item>
                <Button
                    variant="contained"
                    color="default"
                    startIcon={<CloudUpload/>}
                >
                    Upload
                </Button>
            </Grid>
            <Grid item>
                <Button
                    variant="contained"
                    disabled
                    color="secondary"
                    startIcon={<KeyboardVoice/>}
                >
                    Talk
                </Button>
            </Grid>
            <Grid item>
                <Button
                    variant="contained"
                    color="primary"
                    size="small"
                    startIcon={<Save/>}
                >
                    Save
                </Button>
            </Grid>
            <Grid item>
                <Button
                    variant="contained"
                    color="primary"
                    size="large"
                    startIcon={<Save/>}
                >
                    Save
                </Button>
            </Grid>
        </Grid>
    );
});