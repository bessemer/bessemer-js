import React from 'react';
import {storiesOf} from '@storybook/react';
import {Button, Grid} from '@material-ui/core';

let story = storiesOf('Components/Inputs/Button', module);

story.add('Outlined Buttons', () => {
    return (
        <Grid container spacing={3}>
            <Grid item>
                <Button variant="outlined">
                    Default
                </Button>
            </Grid>
            <Grid item>
                <Button variant="outlined" color="primary">
                    Primary
                </Button>
            </Grid>
            <Grid item>
                <Button variant="outlined" color="secondary">
                    Secondary
                </Button>
            </Grid>
            <Grid item>
                <Button variant="outlined" disabled>
                    Disabled
                </Button>
            </Grid>
            <Grid item>
                <Button variant="outlined" href="#" color="primary">
                    Link
                </Button>
            </Grid>
        </Grid>
    );
});
