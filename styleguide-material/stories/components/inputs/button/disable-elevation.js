import React from 'react';
import {storiesOf} from '@storybook/react';
import {Button} from '@material-ui/core';

let story = storiesOf('Components/Inputs/Button', module);

story.add('Disable Elevation', () => {
    return (
        <Button variant="contained" color="primary" disableElevation>
            Disable elevation
        </Button>
    );
});