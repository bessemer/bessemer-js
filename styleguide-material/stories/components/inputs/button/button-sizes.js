import React from 'react';
import {storiesOf} from '@storybook/react';
import {Button, Grid, IconButton} from '@material-ui/core';
import {ArrowDownward, Delete} from '@material-ui/icons';

let story = storiesOf('Components/Inputs/Button', module);

story.add('Button Sizes', () => {
    return (
        <React.Fragment>
            <Grid container spacing={3}>
                <Grid item>
                    <Button size="small">
                        Small
                    </Button>
                </Grid>
                <Grid item>
                    <Button size="medium">
                        Medium
                    </Button>
                </Grid>
                <Grid item>
                    <Button size="large">
                        Large
                    </Button>
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                <Grid item>
                    <Button variant="outlined" size="small" color="primary">
                        Small
                    </Button>
                </Grid>
                <Grid item>
                    <Button variant="outlined" size="medium" color="primary">
                        Medium
                    </Button>
                </Grid>
                <Grid item>
                    <Button variant="outlined" size="large" color="primary">
                        Large
                    </Button>
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                <Grid item>
                    <Button variant="contained" size="small" color="primary">
                        Small
                    </Button>
                </Grid>
                <Grid item>
                    <Button variant="contained" size="medium" color="primary">
                        Medium
                    </Button>
                </Grid>
                <Grid item>
                    <Button variant="contained" size="large" color="primary">
                        Large
                    </Button>
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                <Grid item>
                    <IconButton aria-label="delete" size="small">
                        <ArrowDownward fontSize="inherit"/>
                    </IconButton>
                </Grid>
                <Grid item>
                    <IconButton aria-label="delete">
                        <Delete fontSize="small"/>
                    </IconButton>
                </Grid>
                <Grid item>
                    <IconButton aria-label="delete">
                        <Delete/>
                    </IconButton>
                </Grid>
                <Grid item>
                    <IconButton aria-label="delete">
                        <Delete fontSize="large"/>
                    </IconButton>
                </Grid>
            </Grid>
        </React.Fragment>
    );
});