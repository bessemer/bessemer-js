import React from 'react';

import {storiesOf} from '@storybook/react';
import {Button, Grid} from '@material-ui/core';

let story = storiesOf('Components/Inputs/Button', module);

story.add('Contained Buttons', () => {
    return (
        <Grid container spacing={3}>
            <Grid item>
                <Button variant="contained">
                    Default
                </Button>
            </Grid>
            <Grid item>
                <Button variant="contained" color="primary">
                    Primary
                </Button>
            </Grid>
            <Grid item>
                <Button variant="contained" color="secondary">
                    Secondary
                </Button>
            </Grid>
            <Grid item>
                <Button variant="contained" disabled>
                    Disabled
                </Button>
            </Grid>
            <Grid item>
                <Button variant="contained" color="primary" href="#contained-buttons">
                    Link
                </Button>
            </Grid>
        </Grid>
    );
});