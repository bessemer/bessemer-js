import React from 'react';
import {storiesOf} from '@storybook/react';
import {Fab, Grid} from '@material-ui/core';
import {Add, Navigation} from '@material-ui/icons';

let story = storiesOf('Components/Inputs/Floating Action Button', module);

story.add('Fab Sizes', () => {
    return (
        <React.Fragment>
            <Grid container spacing={3}>
                <Grid item>
                    <Fab size="small" color="secondary" aria-label="add">
                        <Add />
                    </Fab>
                </Grid>
                <Grid item>
                    <Fab size="medium" color="secondary" aria-label="add">
                        <Add />
                    </Fab>
                </Grid>
                <Grid item>
                    <Fab color="secondary" aria-label="add">
                        <Add />
                    </Fab>
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                <Grid item>
                    <Fab
                        variant="extended"
                        size="small"
                        color="primary"
                        aria-label="add"
                    >
                        <Navigation />
                        Extended
                    </Fab>
                </Grid>
                <Grid item>
                    <Fab
                        variant="extended"
                        size="medium"
                        color="primary"
                        aria-label="add"
                    >
                        <Navigation />
                        Extended
                    </Fab>
                </Grid>
                <Grid item>
                    <Fab variant="extended" color="primary" aria-label="add">
                        <Navigation />
                        Extended
                    </Fab>
                </Grid>
            </Grid>
        </React.Fragment>
    );
});