import React from 'react';
import {storiesOf} from '@storybook/react';
import {Fab, Grid} from '@material-ui/core';
import {Add, Edit, Favorite, Navigation} from '@material-ui/icons';

let story = storiesOf('Components/Inputs/Floating Action Button', module);

story.add('Floating Action Button', () => {
    return (
        <Grid container spacing={3}>
            <Grid item>
                <Fab color="primary" aria-label="add">
                    <Add />
                </Fab>
            </Grid>
            <Grid item>
                <Fab color="secondary" aria-label="edit">
                    <Edit />
                </Fab>
            </Grid>
            <Grid item>
                <Fab variant="extended">
                    <Navigation />
                    Navigate
                </Fab>
            </Grid>
            <Grid item>
                <Fab disabled aria-label="like">
                    <Favorite />
                </Fab>
            </Grid>
        </Grid>
    );
});