import React from 'react';
import {storiesOf} from '@storybook/react';
import {Button, ButtonGroup, Grid} from '@material-ui/core';

let story = storiesOf('Components/Inputs/Button Group', module);

story.add('Basic Button Group', () => {
    return (
        <Grid container spacing={3}>
            <Grid item>
                <ButtonGroup color="primary" aria-label="outlined primary button group">
                    <Button>One</Button>
                    <Button>Two</Button>
                    <Button>Three</Button>
                </ButtonGroup>
            </Grid>
            <Grid item>
                <ButtonGroup variant="contained" color="primary" aria-label="contained primary button group">
                    <Button>One</Button>
                    <Button>Two</Button>
                    <Button>Three</Button>
                </ButtonGroup>
            </Grid>
            <Grid item>
                <ButtonGroup variant="text" color="primary" aria-label="text primary button group">
                    <Button>One</Button>
                    <Button>Two</Button>
                    <Button>Three</Button>
                </ButtonGroup>
            </Grid>
        </Grid>
    );
});
