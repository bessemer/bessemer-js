import React from 'react';
import {storiesOf} from '@storybook/react';
import {Button, ButtonGroup, Grid} from '@material-ui/core';

let story = storiesOf('Components/Inputs/Button Group', module);

story.add('Group Sizes and Colors', () => {
    return (
        <Grid container spacing={3}>
            <Grid item>
                <ButtonGroup size="small" aria-label="small outlined button group">
                    <Button>One</Button>
                    <Button>Two</Button>
                    <Button>Three</Button>
                </ButtonGroup>
            </Grid>
            <Grid item>
                <ButtonGroup color="secondary" aria-label="outlined secondary button group">
                    <Button>One</Button>
                    <Button>Two</Button>
                    <Button>Three</Button>
                </ButtonGroup>
            </Grid>
            <Grid item>
                <ButtonGroup size="large" color="primary" aria-label="large outlined primary button group">
                    <Button>One</Button>
                    <Button>Two</Button>
                    <Button>Three</Button>
                </ButtonGroup>
            </Grid>
        </Grid>
    );
});