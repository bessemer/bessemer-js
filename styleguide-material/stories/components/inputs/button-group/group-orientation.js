import React from 'react';
import {storiesOf} from '@storybook/react';
import {Button, ButtonGroup, Grid} from '@material-ui/core';

let story = storiesOf('Components/Inputs/Button Group', module);

story.add('Group Orientation', () => {
    return (
        <Grid container spacing={3}>
            <Grid item>
                <ButtonGroup
                    orientation="vertical"
                    color="primary"
                    aria-label="vertical outlined primary button group"
                >
                    <Button>One</Button>
                    <Button>Two</Button>
                    <Button>Three</Button>
                </ButtonGroup>
            </Grid>
            <Grid item>
                <ButtonGroup
                    orientation="vertical"
                    color="primary"
                    aria-label="vertical contained primary button group"
                    variant="contained"
                >
                    <Button>One</Button>
                    <Button>Two</Button>
                    <Button>Three</Button>
                </ButtonGroup>
            </Grid>
            <Grid item>
                <ButtonGroup
                    orientation="vertical"
                    color="primary"
                    aria-label="vertical contained primary button group"
                    variant="text"
                >
                    <Button>One</Button>
                    <Button>Two</Button>
                    <Button>Three</Button>
                </ButtonGroup>
            </Grid>
        </Grid>
    );
});