import React from 'react';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import {storiesOf} from '@storybook/react';
import CssBaseline from '@material-ui/core/CssBaseline';

let story = storiesOf('Components/Layout/Container', module);

function SimpleContainer() {
    return (
        <React.Fragment>
            <Container maxWidth="sm">
                <Typography component="div" style={{ backgroundColor: '#cfe8fc', height: '100vh' }} />
            </Container>
        </React.Fragment>
    );
}

story.add('Fluid', () => {
    return (
        <SimpleContainer />
    );
});