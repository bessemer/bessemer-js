import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import {storiesOf} from '@storybook/react';

let story = storiesOf('Components/Layout/Container', module);

function FixedContainer() {
    return (
        <React.Fragment>
            <Container fixed>
                <Typography component="div" style={{ backgroundColor: '#cfe8fc', height: '100vh' }} />
            </Container>
        </React.Fragment>
    );
}

story.add('Fixed', () => {
    return (
        <FixedContainer />
    );
});