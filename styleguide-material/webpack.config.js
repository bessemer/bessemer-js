const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

let dist = path.resolve(__dirname, 'build');

module.exports = {
	entry: {
		main: './src/main/js/index.js',
	},
	output: {
		path: path.resolve(dist),
		filename: 'index.js',
		libraryTarget: 'commonjs2',
	},
	module: {
		rules: [
			{
				test: /\.js?$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
				},
			},
		],
	},
	optimization: {
		minimize: false
	}
};