const _ = require('lodash');
const path = require('path');
const MainModule = require('../module');

console.log('=== Resolved Module ===', MainModule);

function webpack(webpackConfig = {}, options = {}) {
    const babelOptions = {
        presets:
            [
                ["@babel/env", {
                    "targets": {
                        "browsers": ["> 1%", "last 2 versions"]
                    }
                }],
                "@babel/preset-react"
            ],
        "plugins": [
            "@babel/plugin-proposal-class-properties",
            "@babel/plugin-proposal-optional-chaining"
        ]
    };

    const { module = {} } = webpackConfig;
    const { loaderOptions, rule = {} } = options;

    return {
        ...webpackConfig,
        module: {
            ...module,
            rules: [
                ...(module.rules || []),
                {
                    test: [/\.stories\.(jsx?$|tsx?$)/],
                    ...rule,
                    enforce: 'pre',
                    use: [
                        {
                            loader: require.resolve('@storybook/source-loader'),
                            options: loaderOptions,
                        },
                        {
                            loader: 'babel-loader',
                            options: babelOptions
                        }
                    ],
                },
            ],
        },
    };
}

module.exports = {
    stories: [
        '../stories/index'
    ],
    addons: [
        '@storybook/addon-storysource/register',
        '@storybook/addon-notes/register',
        '@storybook/addon-viewport/register'
    ],
    webpackFinal: (config) => {
        console.log(config);

        config.module = {
            rules: [
                {
                    test: /\.js?$/,
                    include: _.concat(path.resolve(__dirname, '../'), MainModule.Storybook.paths),
                    use:
                        [
                            {
                                loader: 'babel-loader',
                                options: {
                                    presets:
                                        [
                                            ["@babel/env", {
                                                "targets": {
                                                    "browsers": ["> 1%", "last 2 versions"]
                                                }
                                            }],
                                            "@babel/preset-react"
                                        ],
                                    "plugins": [
                                        "@babel/plugin-proposal-class-properties",
                                        "@babel/plugin-proposal-optional-chaining"
                                    ]
                                }
                            },
                            {
                                loader: '@storybook/source-loader'
                            },
                        ],
                },
                {
                    test: /\.scss$/,
                    // TODO we need to find a way to fix these hardcoded paths
                    include: _.concat(path.resolve(__dirname, '../'), MainModule.Styles.paths),
                    use: [
                        {loader: 'style-loader'},
                        {loader: 'css-loader'},
                        {loader: 'sass-loader'},
                    ]
                }
            ],
        };

        return config;
    },
};