import React from 'react';
import { addDecorator } from '@storybook/react';

import { create } from 'jss';
import rtl from 'jss-rtl';

import { CssBaseline, Container, Box } from '@material-ui/core';
import { createMuiTheme, ThemeProvider, StylesProvider, jssPreset } from '@material-ui/core/styles';

import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

const theme = createMuiTheme({
    direction: 'ltr'
});

// TODO adding the css baseline messes up the grid margins... why?
const MaterialUi = ({children}) => {
    return (
        <React.Fragment>
            <CssBaseline />
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <StylesProvider jss={jss}>
                    <ThemeProvider theme={theme}>
                        <Container>
                            <Box mt={2}>
                                { children }
                            </Box>
                        </Container>
                    </ThemeProvider>
                </StylesProvider>
            </MuiPickersUtilsProvider>
        </React.Fragment>
    );
};

const MaterialDecorator = (storyFn) => (
    <MaterialUi>
        { storyFn() }
    </MaterialUi>
);

addDecorator(MaterialDecorator);