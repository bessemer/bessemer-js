const _ = require('lodash');
const path = require('path');

var name = 'bessemer-material';
exports.name = name;

var Styles = {};

Styles.paths = _.concat(path.resolve(__dirname, '../styles'), []);

exports.Styles = Styles;

var Storybook = {};

Storybook.paths = _.concat(path.resolve(__dirname, '../stories'), []);

Storybook.loadStories = function() {
	require('../stories');
};

exports.Storybook = Storybook;

function webpack(webpackConfig = {}, options = {}) {
	const { module = {} } = webpackConfig;
	const { loaderOptions, rule = {} } = options;

	return {
		...webpackConfig,
		module: {
			...module,
			rules: [
				...(module.rules || []),
				{
					test: [/\.stories\.(jsx?$|tsx?$)/],
					...rule,
					enforce: 'pre',
					use: [
						{
							loader: require.resolve('@storybook/source-loader'),
							options: loaderOptions,
						},
					],
				},
			],
		},
	};
}
